# !/usr/bin/env python  
# -*- coding:utf-8 _*-  
""" 
@author: autocyz
@time: 2019-07-22
@contact: autocyz@163.com
"""
import time
import torch
import os
import sys

sys.path.append(os.getcwd())

import logging
from tensorboardX import SummaryWriter
from torch.utils.data import DataLoader
from torch.optim.lr_scheduler import StepLR
from torchvision.utils import make_grid
from scripts.script_sku.config import args_parse
from simila_paragraph.utils import get_lr, save_params
from simila_paragraph.sample.supplier_dataset import Supplier
from scripts.script_sku.val_sku import val
from simila_paragraph.utils.log import MyLogger
from torch.nn.modules import TripletMarginLoss
from simila_paragraph import network

total_iter = 0


def train(net, trainloader, criterion, optimizer, epoch, writer, logger, device=None):
    display = logger
    if isinstance(display, logging.Logger):
        display = display.info

    time4 = 0
    net.train()
    for i, (anchor, pos, neg) in enumerate(trainloader):
        time4_last = time4
        time0 = time.time()

        if isinstance(device, list):
            anchor = anchor.cuda(device[0])
            pos = pos.cuda(device[0])
            neg = neg.cuda(device[0])

        time1 = time.time()
        anchor_f, pos_f, neg_f, saliency_anchor, saliency_pos, saliency_neg = net(anchor, pos, neg)
        time2 = time.time()

        total_loss = criterion(anchor_f, pos_f, neg_f)
        time3 = time.time()

        optimizer.zero_grad()
        total_loss.backward()
        if total_loss.item() > 1e-6:
            optimizer.step()
        # else:
        #     optimizer.zero_grad()

        time4 = time.time()

        global total_iter
        total_iter += 1
        writer.add_scalar('train', total_loss.item(), total_iter)
        if total_iter % 10 == 0:
            a = anchor[0].cpu()[[2, 1, 0]]
            p = pos[0].cpu()[[2, 1, 0]]
            n = neg[0].cpu()[[2, 1, 0]]
            a_s = saliency_anchor[0].cpu().squeeze()
            a_s = torch.stack([a_s, a_s, a_s])
            p_s = saliency_pos[0].cpu().squeeze()
            p_s = torch.stack([p_s, p_s, p_s])
            n_s = saliency_neg[0].cpu().squeeze()
            n_s = torch.stack([n_s, n_s, n_s])
            print(f"{a.shape}  {p.shape}  {n.shape} {a_s.shape} {p_s.shape} {n_s.shape}")
            triplet = make_grid([a, p, n, a_s, p_s, n_s, a * a_s, p * p_s, n * n_s], nrow=3, normalize=True, padding=10,
                                pad_value=1)
            writer.add_image('triplet', triplet)

        if total_iter % params['display'] == 0:
            logger.info('\nEpoch [{:03d}/{:03d}]\tStep [{}/{}  {:5d}]\tLr [{}] \ttotal_loss {:.4f}\n'
                        'T_pre:{:.5f} T_for:{:.5f} T_loss: {:.5f} T_back:{:.5f}'.
                        format(epoch, params['epoch_num'], i, len(trainloader), total_iter, get_lr(optimizer),
                               total_loss.item(),
                               time0 - time4_last, time2 - time1, time3 - time2, time4 - time3))


if __name__ == "__main__":

    params = args_parse()
    date_dir = os.path.join(params['result_dir'], params['date'])
    logdir = os.path.join(date_dir, 'log')
    cpt_dir = os.path.join(date_dir, 'checkpoint')
    tmp_dir = os.path.join(date_dir, 'tmp')
    os.makedirs(date_dir, exist_ok=True)
    os.makedirs(logdir, exist_ok=True)
    os.makedirs(cpt_dir, exist_ok=True)
    os.makedirs(tmp_dir, exist_ok=True)

    logger = MyLogger(os.path.join(logdir, "train.log"), "simple").get_logger()

    logger.info(">>> loading dataset")
    trainset = Supplier(params['data_dir'], params['train_json'], phase='train',
                        input_size=params['input_size'])
    testset = Supplier(params['data_dir'], params['test_json'], phase='test',
                       input_size=params['input_size'])
    trainloader = DataLoader(trainset, batch_size=params['batch_size'],
                             shuffle=True, num_workers=params['num_workers'], pin_memory=True)
    valloader = DataLoader(testset, batch_size=params['val_batch'],
                           shuffle=False, num_workers=params['num_workers'], pin_memory=True)
    logger.info("<<< loading over")

    params["train_spu_nums"] = len(trainset)
    params['val_spu_nums'] = len(testset)
    save_params(logdir, 'parameter', params)

    net = getattr(network, params['net']['name'])(*params['net']['param'])
    if params['use_gpu']:
        torch.backends.cudnn.enabled = True
        torch.backends.cudnn.benchmark = True
        gpu = params['gpu']
        net = net.cuda(gpu[0])
        if len(gpu) > 1:
            logging.info(f"use multi-gpu: {gpu}")
            net = torch.nn.DataParallel(net, device_ids=gpu)
            # net = net.cuda(gpu[0])
            print(f'net.device_ids: {net.output_device}')

    if params['model']:
        logger.info(f">>> loading pre_trained model : {params['model']}")
        # pretrain_state = torch.load(params['model'])['state_dict']
        # model_dict = net.state_dict()
        # pretrain_state = {k: v for k, v in pretrain_state.items() if k in model_dict}
        # model_dict.update(pretrain_state)
        # net.load_state_dict(model_dict)
        net.load_state_dict(torch.load(params['model'], map_location=lambda storage, loc: storage))
        logger.info("<<< loading over")

    optimizer = torch.optim.Adam(net.parameters(), lr=params['lr'], weight_decay=params['weight_decay'])
    lr_scheduler = StepLR(optimizer, step_size=params['step_size'], gamma=0.1)
    criterion = TripletMarginLoss(margin=params['margin'])
    writer = SummaryWriter(log_dir=logdir)

    best_acc = 0.0
    for epoch in range(params['epoch_start'], params['epoch_num']):
        lr_scheduler.step()
        train(net, trainloader, criterion, optimizer, epoch, writer, logger, device=params['gpu'])
        acc_5, acc_1 = val(net, valloader, epoch, logger, device=params['gpu'])
        writer.add_scalars('val_acc', {'acc_1': acc_1, 'acc_5': acc_5}, epoch)
        if acc_5 > best_acc:
            best_acc = acc_5
            state_dict = net.module.state_dict() if len(params['gpu']) > 1 else net.state_dict()
            torch.save(state_dict, os.path.join(cpt_dir, "epoch_{:03d}_acc_{:.4f}.pth".format(epoch, best_acc)))
