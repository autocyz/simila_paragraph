# !/usr/bin/env python  
# -*- coding:utf-8 _*-  
""" 
@author: autocyz
@time: 2019-07-23
@contact: autocyz@163.com
"""
import logging
import torch
import numpy as np
from sklearn.metrics.pairwise import euclidean_distances


def val(net, valloader, epoch, logger, device='cpu'):
    display = logger
    if isinstance(display, logging.Logger):
        display = display.info
    val_list = valloader.dataset.val_list
    net.eval()
    embeddings = []
    indexes = []
    with torch.no_grad():
        for i, (img, index) in enumerate(valloader):
            if device:
                img = img.to(device)
            vec = net(img)
            vec = vec.cpu().numpy()
            embeddings.append(vec)
            indexes.append(index.numpy())
            if i % 10 == 0:
                display(f"get feature {(i+1)*len(index)}")
    embeddings = np.concatenate(embeddings)
    indexes = np.concatenate(indexes).reshape(-1)

    dis = euclidean_distances(embeddings)

    correct_num = 0
    total_num = 0
    for index in indexes:
        img_name, item, stat = val_list[index]
        if stat != 'query':
            continue
        total_num += 1
        one_dis = dis[index]
        top_ten = one_dis.argsort()[:10]
        top_five = top_ten[1:6]
        display(f'\nimg {img_name} top 5:')
        queryed = False
        for id in top_five:
            if val_list[id][1] == item and not queryed:
                display(f'{val_list[id][0]} {one_dis[id]}')
                correct_num += 1
                queryed = True

    acc = correct_num / total_num
    display(f"acc [{correct_num}/{total_num}] = {acc}")
    return acc


if __name__ == '__main__':
    from simila_paragraph.sample.deep_fashion import DeepFashionInShop
    from torch.utils.data import DataLoader
    from simila_paragraph.network.fashion_net import FashionNet

    root = '/data/cyz_data/DeepFashion/In-shop_Clothes_Retrieval_Benchmark/Img'
    train_split = '/data/cyz_data/DeepFashion/In-shop_Clothes_Retrieval_Benchmark/Eval/list_eval_partition.txt'

    test = DeepFashionInShop(root, train_split, 'test')
    valloader = DataLoader(dataset=test, shuffle=False, batch_size=100)

    net = FashionNet()
    net = net.to('cuda:0')
    val(net, valloader, 0, print, 'cuda:0')
