# !/usr/bin/env python  
# -*- coding:utf-8 _*-  
""" 
@author:autocyz
@license: Apache Licence 
@file: config.py
@time: 2019/04/29
@contact: autoc yz@163.com
@function: 
"""

from argparse import ArgumentParser

params = dict()

# path
params['result_dir'] = 'result'
params['data_dir'] = "/data/cyz_data/DeepFashion/In-shop_Clothes_Retrieval_Benchmark/Img"
# params['train_json'] = "/home/cyz/data/dataset/image_match/train.json"
# params['test_json'] = "/home/cyz/data/dataset/image_match/test.json"
params['train_val_list'] = '/data/cyz_data/DeepFashion/In-shop_Clothes_Retrieval_Benchmark/Eval/list_eval_partition.txt'

# model params
params['input_size'] = (256, 256)
params['margin'] = 20.0
params['inter_weight'] = 1.0
params['intra_weight'] = 1.0
params['model'] = None

# train params
params['epoch_num'] = 50
params['batch_size'] = 16
params['val_batch'] = 100
params['num_workers'] = 8
params['lr'] = 1e-5
params['step_size'] = 20
params['momentum'] = 0.9
params['weight_decay'] = 1e-5
params['display'] = 1
params['use_gpu'] = True
params['gpu'] = 1
params['max_img_num'] = 10
# train log
params['date'] = ""
params['train_log'] = ""


def get_args():
    parse = ArgumentParser()

    parse.add_argument('--date', type=str, default='')
    parse.add_argument('--train_log', type=str, default='')
    parse.add_argument('--model', type=str, default='')

    args = parse.parse_args()
    return args


def args_parse():
    args = get_args()

    if args.date:
        params['date'] = args.date
    if args.train_log:
        params['train_log'] = args.train_log
    if args.model:
        params['model'] = args.model

    return params
