# !/usr/bin/env python  
# -*- coding:utf-8 _*-  
""" 
@author: autocyz
@time: 2019-07-23
@contact: autocyz@163.com
"""
import os
import sys
import torch
sys.path.append(os.getcwd())
from scripts.script_sleeve.val_sleeve import val
from simila_paragraph.sample.deep_fashion_attr import AttrSleeve
from torch.utils.data import DataLoader
from simila_paragraph.network import NASnetLarge

def main():
    root = "/data/open_data/DeepFashion/Category_and_Attribute_Prediction_Benchmark/Img"
    file_list = "script_sleeve/val.txt"
    model = 'result/attribute/0809/checkpoint/epoch_001_acc_0.9451.pth'
    dataset = AttrSleeve(root, file_list)
    print(f"dataset have {len(dataset)} imgs")
    dataloader = DataLoader(dataset, shuffle=False, batch_size=64, num_workers=4)
    net = NASnetLarge(1)
    print(f">>>> load pretrained model")
    net.load_state_dict(torch.load(model, map_location=lambda storage, loc: storage))
    print(f"<<<< load pretrained model over")
    net.cuda(0)
    val(net, dataloader, 0, print, device='cuda:0')


if __name__ == '__main__':
    # feature_val()
    main()
