# !/usr/bin/env python  
# -*- coding:utf-8 _*-  
""" 
@author: autocyz
@time: 2019-07-23
@contact: autocyz@163.com
"""
import torch.nn as nn
from torchvision.models.resnet import resnet50
import torch


class FashionNet(nn.Module):
    def __init__(self, pretrained=True):
        super(FashionNet, self).__init__()
        self.backbone = resnet50(pretrained=pretrained)
        self.embedding = nn.Sequential(
            nn.ReLU(inplace=True),
            nn.Linear(1000, 500)
        )

    def forward(self, *input):
        if len(input) > 1:
            return self._train(*input)
        else:
            return self._inference(*input)

    def _inference(self, input):
        y = self.backbone(input)
        y = self.embedding(y)
        return y

    def _train(self, *input):
        anchor = input[0]
        pos = input[1]
        neg = input[2]

        anchor_f = self._inference(anchor)
        pos_f = self._inference(pos)
        neg_f = self._inference(neg)
        return anchor_f, pos_f, neg_f
