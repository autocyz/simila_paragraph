# !/usr/bin/env python  
# -*- coding:utf-8 _*-  
""" 
@author: autocyz
@time: 2019-07-15
@contact: autocyz@163.com
"""
import os
import numpy as np
import matplotlib.pyplot as plt
from argparse import ArgumentParser


def main():

    parse = ArgumentParser()
    parse.add_argument('--root', type=str, default='')
    parse.add_argument('--epoch', type=str, default="000")
    args = parse.parse_args()

    epoch = args.epoch
    root = args.root
    save_path = os.path.join(root, 'figure')
    os.makedirs(save_path, exist_ok=True)

    intra_npy = os.path.join(root, epoch + "_intra.npy")
    inter_pos_npy = os.path.join(root, epoch + "_inter_pos.npy")
    inter_neg_npy = os.path.join(root, epoch + "_inter_neg.npy")

    intra = np.load(intra_npy)
    inter_pos = np.load(inter_pos_npy)
    inter_neg = np.load(inter_neg_npy)

    plt.hist(intra, bins='auto', alpha=0.5, label='intra', color='red')
    plt.hist(inter_pos, bins='auto', alpha=0.5, label='inter_pos', color='green')
    plt.hist(inter_neg, bins='auto', alpha=0.5, label='inter_neg', color='blue')
    plt.xlabel(epoch)
    plt.grid(axis='y', alpha=0.5)
    plt.legend(loc='upper right')
    plt.savefig(os.path.join(save_path, epoch + '.png'))
    plt.show()


if __name__ == '__main__':
    main()
