# !/usr/bin/env python  
# -*- coding:utf-8 _*-  
""" 
@author: autocyz
@time: 2019-07-10
@contact: autocyz@163.com
"""

import torch
import numpy as np
import torch.nn as nn


def pairwise_distances(x, y=None):
    """
    Input: x is a Nxd matrix
           y is an optional Mxd matirx
    Output: dist is a NxM matrix where dist[i,j] is the square norm between x[i,:] and y[j,:]
            if y is not given then use 'y=x'.
    i.e. dist[i,j] = ||x[i,:]-y[j,:]||^2
    """
    x_norm = (x ** 2).sum(1).view(-1, 1)
    if y is not None:
        y_t = torch.transpose(y, 0, 1)
        y_norm = (y ** 2).sum(1).view(1, -1)
    else:
        y_t = torch.transpose(x, 0, 1)
        y_norm = x_norm.view(1, -1)

    dist = x_norm + y_norm - 2.0 * torch.mm(x, y_t)
    # Ensure diagonal is zero if x=y
    # if y is None:
    #     dist = dist - torch.diag(dist.diag)
    return torch.clamp(dist, 0.0, np.inf)


class SpuLoss(nn.Module):
    def __init__(self,  inter_weight=1.0, intra_weight=1.0, margin=3):
        super(SpuLoss, self).__init__()
        self.inter_w = inter_weight
        self.intra_w = intra_weight
        self.margin = margin

    def forward(self, *input):
        assert len(input) >= 3
        anchor = input[0]
        pos = input[1]
        neg = input[2]

        anchor_intra_loss = torch.nn.functional.pdist(anchor, p=2)
        pos_intra_loss = torch.nn.functional.pdist(pos, p=2)
        neg_intra_loss = torch.nn.functional.pdist(neg, p=2)

        # print('\n')
        # print(f"anchor_intra: {anchor_intra_loss.shape} pos_intra: {pos_intra_loss.shape} neg_intra: {neg_intra_loss.shape}")

        if anchor_intra_loss.shape[0] == 0:
            anchor_intra_loss = torch.sum(anchor_intra_loss)
        else:
            anchor_intra_loss = torch.sum(anchor_intra_loss) / anchor_intra_loss.shape[0]
        if pos_intra_loss.shape[0] == 0:
            pos_intra_loss = torch.sum(pos_intra_loss)
        else:
            pos_intra_loss = torch.sum(pos_intra_loss) / pos_intra_loss.shape[0]
        if neg_intra_loss.shape[0] == 0:
            neg_intra_loss = torch.sum(neg_intra_loss)
        else:
            neg_intra_loss = torch.sum(neg_intra_loss) / neg_intra_loss.shape[0]

        anchor_pos_loss = pairwise_distances(anchor, pos)
        anchor_neg_loss = pairwise_distances(anchor, neg)
        # print(f"anchor_pos: {anchor_pos_loss.shape} anchor_neg: {anchor_neg_loss.shape}")
        anchor_pos_loss = torch.sum(anchor_pos_loss) / (anchor_pos_loss.shape[0] * anchor_pos_loss.shape[1])
        anchor_neg_loss = torch.sum(anchor_neg_loss) / (anchor_neg_loss.shape[0] * anchor_neg_loss.shape[1])
        # print(f"anchor_pos: {anchor_pos_loss} anchor_neg: {anchor_neg_loss}")

        intra_loss = anchor_intra_loss + pos_intra_loss + neg_intra_loss
        inter_loss = torch.nn.functional.relu(anchor_pos_loss + self.margin - anchor_neg_loss)

        total_loss = self.intra_w * intra_loss + self.inter_w * inter_loss
        return total_loss, intra_loss, inter_loss, anchor_pos_loss, anchor_neg_loss


if __name__ == '__main__':
    anchor = torch.randn(5, 10)
    neg = torch.randn(7, 10)
    pos = torch.randn(7, 10)

    spuloss = SpuLoss()
    print(111111)
    l = spuloss(anchor, pos, neg)
    print(l)
