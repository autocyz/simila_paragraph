# !/usr/bin/env python  
# -*- coding:utf-8 _*-  
""" 
@author: autocyz
@file: spu_match.py 
@time: 2019-05-14
@contact: autocyz@163.com
@function: 
"""

import os
import json
import numpy as np
import random
import cv2
from torch.utils.data import Dataset

# used_category = ['auto', 'beauty', 'fashion_jewelry',  'mens_bags',  'mens_shoes',
#                  'sports', 'womens_bags', 'womens_shoes', 'baby', 'electronics',
#                  'home_garden', 'mens_clothing', 'office', 'toys', 'watches', 'womens_clothing']

used_category = ['mens_clothing', 'womens_clothing']


class SpuDataset(Dataset):
    def __init__(self, root, data_json, is_train=True, input_size=(256, 256), max_img_num=10):
        super(SpuDataset, self).__init__()
        self.load_mode = "train" if is_train else "test"
        self.root = root
        self.input_size = input_size
        self.data_dict = json.load(open(data_json, 'r'))

        self.max_img_num = max_img_num

        self.spus = []
        for pspu, data in self.data_dict.items():
            category = pspu.split('/')[1]
            if category not in used_category:
                continue
            # if len(data) < 3:
            #     continue
            for spu in data:
                self.spus.append(pspu + '/' + spu)
        print(f"{self.load_mode} spu nums: {len(self.spus)}")

        self.__load_data = self.__load_train if is_train else self.__load_test

    def __load_train(self, index):
        spu = self.spus[index]
        anchor_pspu = spu[0: spu.rfind('/')]

        anchor_spu = spu[spu.rfind('/') + 1:]
        anchor_cat = spu.split('/')[1]
        pos_spus = list(self.data_dict[anchor_pspu].keys())

        pos_spu = anchor_spu
        while pos_spu == anchor_spu and len(pos_spus) != 1:
            pos_spu = random.choice(pos_spus)

        # 选取neg spu时要求其category与anchor属于同一个cag
        # neg_spu = spu
        # neg_cat = neg_spu[0:neg_spu.find('/')]
        # neg_pspu = neg_spu[0: neg_spu.rfind('/') + 1]
        # while neg_pspu == pspu or anchor_cat != neg_cat:
        #     neg_spu = random.choice(self.spus)
        #     neg_cat = neg_spu[0:neg_spu.find('/')]
        #     neg_pspu = neg_spu[0: neg_spu.rfind('/') + 1]

        # 选取neg spu不要求其category要与anchor属于同一个cag
        neg_spu = random.choice(self.spus)
        neg_pspu = neg_spu[0: neg_spu.rfind('/')]
        while neg_pspu == anchor_pspu:
            neg_spu = random.choice(self.spus)
            neg_pspu = neg_spu[0: neg_spu.rfind('/')]

        neg_spu = neg_spu[neg_spu.rfind('/') + 1:]

        anchor_img_names = self.data_dict[anchor_pspu][anchor_spu]
        pos_img_names = self.data_dict[anchor_pspu][pos_spu]
        neg_img_names = self.data_dict[neg_pspu][neg_spu]

        if len(anchor_img_names) > self.max_img_num:
            anchor_img_names = random.sample(anchor_img_names, self.max_img_num)
        if len(pos_img_names) > self.max_img_num:
            pos_img_names = random.sample(pos_img_names, self.max_img_num)
        if len(neg_img_names) > self.max_img_num:
            neg_img_names = random.sample(neg_img_names, self.max_img_num)

        anchor_imgs = [self._load_img(os.path.join(self.root, name), self.input_size) for name in anchor_img_names]
        pos_imgs = [self._load_img(os.path.join(self.root, name), self.input_size) for name in pos_img_names]
        neg_imgs = [self._load_img(os.path.join(self.root, name), self.input_size) for name in neg_img_names]

        return anchor_imgs, pos_imgs, neg_imgs, anchor_img_names, pos_img_names, neg_img_names

    def __load_test(self, index):
        spu = self.spus[index]
        pspu = spu[0: spu.rfind('/')]
        spu = spu[spu.rfind('/') + 1:]

        img_names = self.data_dict[pspu][spu]
        img_names = img_names[0:self.max_img_num]

        imgs = [self._load_img(os.path.join(self.root, name), self.input_size) for name in img_names]

        return pspu, spu, imgs

    @staticmethod
    def _load_img(img_name, size):
        img = cv2.imread(img_name)
        if img is None:
            print(f"!!!!!!!!!!!!!!!!\n{img_name} is None")
        img = cv2.resize(img, size)
        img = np.transpose(img, (2, 0, 1)) / 255
        img = img.astype(np.float32)
        return img

    def __getitem__(self, index):
        return self.__load_data(index)

    def __len__(self):
        return len(self.spus)


if __name__ == '__main__':
    root = "/data/cyz_data/dataset/image_match"
    train_json = "/data/cyz_data/dataset/image_match/train.json"
    test_json = "/data/cyz_data/dataset/image_match/test.json"

    trainset = SpuDataset(root, train_json, True)
    testset = SpuDataset(root, test_json, False)

    for i in range(1):
        a = random.randint(0, len(trainset))
        _, _, _, anchor, pos, neg = trainset.__getitem__(a)
        print([a.split('/')[2] + '/' + a.split('/')[3] for a in anchor])
        print([a.split('/')[2] + '/' + a.split('/')[3] for a in pos])
        print([a.split('/')[2] + '/' + a.split('/')[3] for a in neg])
        print("===" * 10)
    # _, _, _, anchor, pos, neg = next(testset)
