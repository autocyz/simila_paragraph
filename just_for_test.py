# !/usr/bin/env python  
# -*- coding:utf-8 _*-  

import cv2
import os
import json
from tqdm import tqdm
from collections import defaultdict

import cv2
import os
from tqdm import tqdm
from concurrent.futures import ProcessPoolExecutor
import concurrent
import traceback
import random

# root = 'seller_images'
# with open('seller.txt', 'r') as f:
#     img_list = f.readlines()
#     img_list = [s.strip() for s in img_list]
# start = 100000
# img_list = img_list[start: start + 10000]

root = r'/data/open_data/DeepFashion/Category_and_Attribute_Prediction_Benchmark/Img'
with open('script_sleeve/all.txt', 'r') as f:
    sleeve = f.readlines()

# for one in sleeve:
#     img = one.split()[0]
#     if not os.path.exists(os.path.join(root, img)):
#         print(f"{img} img none")


random.shuffle(sleeve)
split_num = int(0.8*len(sleeve))
train = sleeve[:split_num]
val = sleeve[split_num:]

with open('script_sleeve/train.txt', 'w') as f:
    f.writelines(train)
with open('script_sleeve/val.txt', 'w') as f:
    f.writelines(val)
