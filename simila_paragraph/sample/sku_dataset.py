# !/usr/bin/env python  
# -*- coding:utf-8 _*-  
""" 
@author: autocyz
@time: 2019-08-02
@contact: autocyz@163.com
"""

import cv2
import torch
import os
import cv2
import numpy as np
import random
import json
from torch.utils.data import Dataset
from collections import defaultdict
from os.path import join as oj


class SkuSimilar(Dataset):
    def __init__(self, root, file_json, phase, input_size=(256, 256)):
        super(SkuSimilar, self).__init__()
        self.root = root
        self.phase = phase
        self.input_size = input_size

        with open(file_json, 'r') as f:
            self.data_dict = json.load(f)

        self.data_dict_new = defaultdict(list)
        self.data_list = []
        for cluster, skus in self.data_dict.items():
            for sku, imgs in skus.items():
                if len(imgs) < 1:
                    print(f'{cluster} {sku} have no img')
                    continue
                self.data_list += [[img[0], cluster, img[1]] for img in imgs]
                # self.data_dict_new[cluster] += [img[0] for img in imgs]
                for img in imgs:
                    self.data_dict_new[cluster].append(img[0])
        print(f"{self.phase} dataset have {len(self.data_dict)} items, {len(self.data_list)} imgs")
        self.items = list(self.data_dict_new.keys())

        self._load_data = self._load_train if self.phase == 'train' else self._load_test

    def _load_img(self, img_name, size):
        img = cv2.imread(img_name)
        if img is None:
            print(f'img {img_name} is None')
            raise RuntimeError(f'img {img_name} is None')
        img = cv2.resize(img, size)
        img = np.transpose(img, (2, 0, 1)) / 255
        img = img.astype(np.float32)
        return img

    def _load_train(self, index):
        anchor_img, anchor_item, phase = self.data_list[index]
        anchor_cat = anchor_item.split('_')[0]
        pos_img = anchor_img
        if len(self.data_dict_new[anchor_item]) > 1:
            while pos_img == anchor_img:
                pos_img = random.choice(self.data_dict_new[anchor_item])

        neg_item = anchor_item
        neg_cat = anchor_cat
        # while neg_item == anchor_item:
        while neg_item == anchor_item or neg_cat != anchor_cat:
            neg_item = random.choice(self.items)
            neg_cat = neg_item.split('_')[0]
        neg_img = random.choice(self.data_dict_new[neg_item])
        anchor = self._load_img(oj(self.root, anchor_img), self.input_size)
        pos = self._load_img(oj(self.root, pos_img), self.input_size)
        neg = self._load_img(oj(self.root, neg_img), self.input_size)

        # print(f'{anchor_img}')
        # print(f'{pos_img}')
        # print(f'{neg_img}\n')
        return anchor, pos, neg

    def _load_test(self, index):
        img = self.data_list[index][0]
        img = self._load_img(oj(self.root, img), self.input_size)
        return img, index

    def __getitem__(self, index):
        return self._load_data(index)

    def __len__(self):
        return len(self.data_list)


if __name__ == '__main__':
    from torch.utils.data import DataLoader

    root = '/data/open_data/similiar_dataset/sku_dataset'
    file_json = '/data/open_data/similiar_dataset/sku_dataset/train_list.json'
    phase = 'train'

    dataset = SkuSimilar(root, file_json, phase)
    dataloader = DataLoader(dataset, 1, True, num_workers=1, pin_memory=True)
    # data = next(dataloader)
    for i, data in enumerate(dataloader):
        # print(data.shape)
        if i > 10:
            break
