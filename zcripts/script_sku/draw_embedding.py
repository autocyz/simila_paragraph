# !/usr/bin/env python  
# -*- coding:utf-8 _*-  
""" 
@author: autocyz
@time: 2019-09-05
@contact: autocyz@163.com
"""

from sklearn.manifold import TSNE
import os
import glob
import random
import json
import cv2
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import NullFormatter
from matplotlib.offsetbox import OffsetImage, AnnotationBbox
from PIL import Image


def get_data():
    embedding_root = '/data/open_data/similar_gallery/clubfactory_online/embedding/607'
    img_root = '/data/open_data/similar_gallery/clubfactory_online/images/607'

    feature_file = 'random_2000_features.npy'
    img_file = 'random_2000_imgs.txt'
    already_have = False
    # already_have = True

    if not already_have:
        random_num = 2000
        embeddings = glob.glob(embedding_root + '/**/*.npy', recursive=True)
        embeddings = random.sample(embeddings, random_num)
        imgs = [s.replace('embedding', 'images')[:-4] for s in embeddings]
        print(f'>>> loading {len(embeddings)} features')
        features = [np.load(s).reshape(1, -1) for s in embeddings]
        features = np.concatenate(features)
        np.save(f'random_{random_num}_features', features)
        open(f'random_{random_num}_imgs.txt', 'w').writelines([img + '\n' for img in imgs])
        print(f"<<< loading over")
        return features, imgs
    else:
        features = np.load(feature_file)
        imgs = open(img_file, 'r').readlines()
        imgs = [s.strip() for s in imgs]
        return features, imgs


def draw_tsne(features, imgs):
    print(f">>> t-SNE fitting")
    tsne = TSNE(n_components=2, init='pca', perplexity=30)
    # fig.set_size_inches(18.5, 10.5)
    Y = tsne.fit_transform(features)
    print(f"<<< fitting over")

    fig, ax = plt.subplots()
    fig.set_size_inches(21.6, 14.4)
    # plt.scatter(Y[:, 0], Y[:, 1])
    # plt.xaxis.set_major_formatter(NullFormatter())
    # plt.yaxis.set_major_formatter(NullFormatter())
    plt.axis('off')
    print(f">>> plotting images")
    imscatter(Y[:, 0], Y[:, 1], imgs, zoom=0.1, ax=ax)
    print(f"<<< plot over")
    plt.savefig(fname='figure.eps', format='eps')
    # plt.show()


def imscatter(x, y, images, ax=None, zoom=1):
    if ax is None:
        ax = plt.gca()
    x, y = np.atleast_1d(x, y)
    artists = []
    for x0, y0, image in zip(x, y, images):
        im = cv2.imread(image)
        im = cv2.resize(im, (300, 300))
        im = cv2.cvtColor(im, cv2.COLOR_BGR2RGB)
        # im = plt.imread(image)
        im_f = OffsetImage(im, zoom=zoom)
        ab = AnnotationBbox(im_f, (x0, y0), xycoords='data', frameon=False)
        artists.append(ax.add_artist(ab))
    ax.update_datalim(np.column_stack([x, y]))
    ax.autoscale()
    return artists


def main():
    f, i = get_data()
    draw_tsne(f, i)


if __name__ == '__main__':
    main()
