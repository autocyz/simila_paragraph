# !/usr/bin/env python  
# -*- coding:utf-8 _*-  
""" 
@author: autocyz
@time: 2019-08-08
@contact: autocyz@163.com
"""
from torch.utils.data import Dataset
from collections import defaultdict
import os
import cv2
import numpy as np
import random


class AttrSleeve(Dataset):
    def __init__(self, root, file_list, input_size=(331, 331)):
        super(AttrSleeve, self).__init__()

        self.root = root
        self.input_size = input_size
        with open(file_list, 'r') as f:
            img_list = f.readlines()
        self.img_list = [img.strip().split() for img in img_list]

    @staticmethod
    def _load_img(img_name, size):
        img = cv2.imread(img_name)
        if img is None:
            print(f"!!!!!!!!!!!!!!!!\n{img_name} is None")
        img = cv2.resize(img, size)
        img = np.transpose(img, (2, 0, 1)) / 255
        img = img.astype(np.float32)
        return img

    def __getitem__(self, index):
        img_path, label = self.img_list[index]
        img_path = os.path.join(self.root, img_path)
        label = float(int(label))
        img = self._load_img(img_path, self.input_size)
        return img, label

    def __len__(self):
        return len(self.img_list)


class OneClassSleeve(Dataset):
    def __init__(self, root, file_list, phase='train', input_size=(331, 331)):
        super(OneClassSleeve, self).__init__()
        self.root = root
        self.phase = phase
        self.input_size = input_size
        with open(file_list, 'r') as f:
            img_list = f.readlines()

        self.img_list = [img.strip().split() for img in img_list]

    @staticmethod
    def _load_img(img_name, size):
        img = cv2.imread(img_name)
        if img is None:
            print(f"!!!!!!!!!!!!!!!!\n{img_name} is None")
        img = cv2.resize(img, size)
        img = np.transpose(img, (2, 0, 1)) / 255
        img = img.astype(np.float32)
        return img

    def __getitem__(self, index):
        img_path, label = self.img_list[index]
        img_path = os.path.join(self.root, img_path)
        label = float(int(label))
        img = self._load_img(img_path, self.input_size)
        if self.phase != 'train':
            return img, label

        fake = np.random.normal(0, 0.1, (1000, ))
        return img, label, fake, 0.0

    def __len__(self):
        return len(self.img_list)


if __name__ == '__main__':
    root = '/data/open_data/DeepFashion/Category and Attribute Prediction Benchmark/Img'
    file_list = '/home/cyz/data/code/simila_paragraph/script_sleeve/all.txt'
    att = AttrSleeve(root, file_list)
