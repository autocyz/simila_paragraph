import os


def save_params(path, name, params):
    with open(os.path.join(path, name+'.txt'), 'w') as f:
        for key, val in params.items():
            print(key, ' : ', val)
            f.write(key + ': ')
            f.write(str(val))
            f.write('\n')


def get_lr(optimizer):
    for param_group in optimizer.param_groups:
        return param_group['lr']


def accuracy(dista, distb):
    margin = 0
    pred = (dista - distb - margin).cpu().data
    return (pred > 0).sum()*1.0/dista.size()[0]


def triplet_correct(dist_n, dist_p):
    margin = 0
    pred = (dist_n - dist_p - margin).cpu().data
    return (pred > 0).sum()


class AverageMeter(object):
    """Computes and stores the average and current value"""
    def __init__(self):
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count


if __name__ == '__main__':
    from config import params_transform
    save_params('./', 'params_transform', params_transform)
