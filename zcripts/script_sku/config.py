# !/usr/bin/env python  
# -*- coding:utf-8 _*-  
""" 
@author:autocyz
@license: Apache Licence 
@file: config.py
@time: 2019/04/29
@contact: autoc yz@163.com
@function: 
"""

from argparse import ArgumentParser

params = dict()

# path
params['result_dir'] = 'result/embedding'
# params['data_dir'] = "/data/open_data/similiar_dataset/sku_dataset"
# params['train_json'] = '/data/open_data/similiar_dataset/sku_dataset/train_list.json'
# params['test_json'] = '/data/open_data/similiar_dataset/sku_dataset/test_list.json'
params['data_dir'] = "/data/open_data/supplier_unique/resized256/labeled"
params['train_json'] = '/data/open_data/supplier_unique/train.json'
params['test_json'] = '/data/open_data/supplier_unique/test_simple.json'

# model params
# params['net'] = {'name': 'SaliencyEmbedding',
#                  'param': ['script_sku/final.pth']}
params['net'] = {'name': 'SaliencyEmbedding',
                 'param': []}
params['input_size'] = (256, 256)
# params['input_size'] = (224, 224)
params['margin'] = 20.0
params['model'] = None

# train params
params['epoch_num'] = 60
params['epoch_start'] = 0
params['batch_size'] = 1
params['val_batch'] = 64
params['num_workers'] = 8
params['lr'] = 1e-4
params['step_size'] = 30
params['momentum'] = 0.9
params['weight_decay'] = 1e-5
params['display'] = 10
params['use_gpu'] = True
params['gpu'] = [0]

# train log
params['date'] = ""
params['train_log'] = ""


def get_args():
    parse = ArgumentParser()

    parse.add_argument('--date', type=str, default='')
    parse.add_argument('--train_log', type=str, default='')
    parse.add_argument('--model', type=str, default='')
    parse.add_argument('--lr', type=float)
    args = parse.parse_args()
    return args


def args_parse():
    args = get_args()

    if args.date:
        params['date'] = args.date
    if args.train_log:
        params['train_log'] = args.train_log
    if args.model:
        params['model'] = args.model
    if args.lr:
        params['lr'] = args.lr

    return params
