# !/usr/bin/env python  
# -*- coding:utf-8 _*-  
""" 
@author: autocyz
@time: 2019-07-11
@contact: autocyz@163.com
"""
from tqdm import tqdm
import os
import random
import json


def split_dataset():
    relative_path = "/home/cyz/data/dataset/image_match"
    roots = ["/home/cyz/data/dataset/image_match/haveGT",
             "/home/cyz/data/dataset/image_match/mongo_day0",
             "/home/cyz/data/dataset/image_match/mongo_day1"]

    file_save_root = "/home/cyz/data/dataset/image_match"

    roots_tq = tqdm(roots)

    train_dict = {}
    test_dict = {}
    for root in roots_tq:
        roots_tq.set_description_str(root)
        cags = os.listdir(root)
        for cag in cags:
            cag_path = os.path.join(root, cag)
            clusters = os.listdir(cag_path)
            random.shuffle(clusters)
            for idx, cluster in enumerate(clusters):
                spu_dict = {}
                cluster_path = os.path.join(cag_path, cluster)
                cluster_id = os.path.relpath(cluster_path, relative_path)

                roots_tq.write(f"cluster: {cluster_id}")

                imgs = os.listdir(cluster_path)
                for img in imgs:
                    spu = img.split('_')[0]
                    img_path = os.path.join(cluster_path, img)
                    if spu in spu_dict:
                        spu_dict[spu].append(os.path.relpath(img_path, relative_path))
                    else:
                        spu_dict[spu] = [os.path.relpath(img_path, relative_path)]

                if idx < len(clusters) * 0.8:
                    train_dict[cluster_id] = spu_dict
                else:
                    test_dict[cluster_id] = spu_dict

    print(f"train_list have {len(train_dict)} cluster\n"
          f"test_list have {len(test_dict)} cluster")
    with open(os.path.join(file_save_root, "train.json"), 'w') as f:
        json.dump(train_dict, f, ensure_ascii=False, indent=4)

    with open(os.path.join(file_save_root, "test.json"), 'w') as f:
        json.dump(test_dict, f, ensure_ascii=False, indent=4)


if __name__ == '__main__':
    split_dataset()
