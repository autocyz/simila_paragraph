# !/usr/bin/env python  
# -*- coding:utf-8 _*-  
""" 
@author: autocyz
@time: 2019-07-23
@contact: autocyz@163.com
"""
import logging
import torch
import numpy as np
from sklearn.metrics.pairwise import euclidean_distances
import os
import sys

sys.path.append(os.getcwd())


def val(net, valloader, epoch, logger, device=None):
    display = logger
    if isinstance(display, logging.Logger):
        display = display.info
    val_list = valloader.dataset.data_list
    net.eval()
    embeddings = []
    indexes = []
    with torch.no_grad():
        for i, (img, index) in enumerate(valloader):
            if isinstance(device, list):
                img = img.cuda()
            vec = net(img)
            vec = vec.cpu().numpy()
            embeddings.append(vec)
            indexes.append(index.numpy())
            if i % 10 == 0:
                display(f"get feature {(i + 1) * len(index)}")
    embeddings = np.concatenate(embeddings)
    indexes = np.concatenate(indexes).reshape(-1)

    dis = euclidean_distances(embeddings)

    correct_num = 0
    correct_num_one = 0
    total_num = 0
    for index in indexes:
        img_name, item, stat = val_list[index]
        if stat != 'query':
            continue
        total_num += 1
        one_dis = dis[index]
        top_ten = one_dis.argsort()[:10]
        top_five = top_ten[1:6]

        if val_list[top_five[0]][1] == item:
            correct_num_one += 1

        display(f'\nimg {img_name} top 5:')
        queryed = False
        for id in top_five:
            if val_list[id][1] == item and not queryed:
                display(f'{val_list[id][0]} {one_dis[id]}')
                correct_num += 1
                queryed = True

    acc_5 = correct_num / total_num
    acc_1 = correct_num_one / total_num
    display(f"top5: acc [{correct_num}/{total_num}] = {acc_5}\n"
            f"top1: acc [{correct_num_one}/{total_num}] = {acc_1}")
    return acc_5, acc_1


if __name__ == '__main__':
    from torch.utils.data import DataLoader
    from simila_paragraph.network.fashion_net import FashionNet
    from simila_paragraph.sample import SkuSimilar

    root = '/data/open_data/similar_style'
    train_split = '/data/open_data/similar_style/test_list.json'

    test = SkuSimilar(root, train_split, 'test')
    valloader = DataLoader(dataset=test, shuffle=False, batch_size=100, pin_memory=True)

    net = FashionNet()
    net = net.to('cuda:0')
    val(net, valloader, 0, print, 'cuda:0')
