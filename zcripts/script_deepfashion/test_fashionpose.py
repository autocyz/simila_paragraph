# !/usr/bin/env python  
# -*- coding:utf-8 _*-  
""" 
@author: autocyz
@time: 2019-07-23
@contact: autocyz@163.com
"""
from simila_paragraph.network.fashion_net import FashionNet
from simila_paragraph.sample.deep_fashion import DeepFashionInShop
import cv2
import os
import torch
import numpy as np
from torch.utils.data import DataLoader
from sklearn.metrics.pairwise import euclidean_distances


def feature_val():
    root = '/data/cyz_data/DeepFashion/In-shop_Clothes_Retrieval_Benchmark/Img'
    train_split = '/data/cyz_data/DeepFashion/In-shop_Clothes_Retrieval_Benchmark/Eval/list_eval_partition.txt'
    model = "/home/cyz/data/code/simila_paragraph/result/0724/checkpoint/epoch_049_acc_0.7562.pth"
    test = DeepFashionInShop(root, train_split, 'test')
    valloader = DataLoader(dataset=test, shuffle=False, batch_size=100, num_workers=8)

    net = FashionNet()
    net.load_state_dict(torch.load(model))
    net.to('cuda:0')
    net.eval()

    embeddings = []
    indexes = []
    val_list = test.val_list
    with torch.no_grad():
        for i, (img, index) in enumerate(valloader):
            img = img.to('cuda:0')
            vec = net(img)
            vec = vec.cpu().numpy()
            embeddings.append(vec)
            indexes.append(index.numpy())
            if i % 10 == 0:
                print(f"get {(i + 1) * len(index)}")

    embeddings = np.concatenate(embeddings)
    np.save('trash/fashion_pose_val_vecs', embeddings)


def view_query():
    save_root = 'trash/query_res'
    root = '/data/cyz_data/DeepFashion/In-shop_Clothes_Retrieval_Benchmark/Img'
    train_split = '/data/cyz_data/DeepFashion/In-shop_Clothes_Retrieval_Benchmark/Eval/list_eval_partition.txt'
    test = DeepFashionInShop(root, train_split, 'test')
    val_list = test.val_list
    all_feature = np.load('trash/fashion_pose_val_vecs.npy')
    print(f">>> compute distance")
    all_dis = euclidean_distances(all_feature)
    np.save('trash/fashion_pos_val_dis.npy', all_dis)
    print(f"<<< compute over")

    # print(f">>> load distance")
    # all_dis = np.load('trash/fashion_pos_val_dis.npy')
    # print(f">>> load over")
    correct_num = 0
    all_num = 0
    for i in range(len(val_list)):
        if val_list[i][2] != 'query':
            continue
        all_num += 1
        dis = all_dis[i]
        top_ten = dis.argsort()[:10]
        top_five = top_ten[1:6]
        query_img, query_item = val_list[i][0:2]
        print(f"query {query_img}")
        show_imgs = []
        show_imgs.append(cv2.imread(os.path.join(root, query_img)))
        queryed = False
        for j in top_five:
            img_name, item = val_list[j][0:2]
            d = dis[j]
            if item == query_item and not queryed:
                correct_num += 1
                queryed = True
            img = cv2.imread(os.path.join(root, img_name))
            d = '%.03f' % d
            cv2.putText(img, d, (30, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255))
            show_imgs.append(img)
        result_img = np.concatenate(show_imgs, axis=1)
        # cv2.imshow('img', result_img)
        # cv2.waitKey(0)
        cv2.imwrite(os.path.join('trash/query_res', query_img.replace('/', '_')), result_img)
        print(f"acc {correct_num / all_num}")


if __name__ == '__main__':
    # feature_val()
    view_query()
