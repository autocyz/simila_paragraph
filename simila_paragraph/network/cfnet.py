# !/usr/bin/env python  
# -*- coding:utf-8 _*-  
""" 
@author: autocyz
@time: 2019-07-10
@contact: autocyz@163.com
"""
from simila_paragraph.network.hourglass import Hourglass
from simila_paragraph.network.hourglass import Bottleneck
from torchvision.models.resnet import resnet50
import torch.nn as nn
import torch.nn.functional as F


class RegionCropNet(nn.Module):

    def __init__(self, block, num_stacks=1, num_blocks=1, num_classes=1):
        super(RegionCropNet, self).__init__()

        self.inplanes = 64
        self.num_feats = 128
        self.num_stacks = num_stacks
        self.conv1 = nn.Conv2d(3, self.inplanes, kernel_size=7, stride=2, padding=3,
                               bias=True)
        self.bn1 = nn.BatchNorm2d(self.inplanes)
        self.relu = nn.ReLU(inplace=True)
        self.layer1 = self._make_residual(block, self.inplanes, 1)
        self.layer2 = self._make_residual(block, self.inplanes, 1)
        self.layer3 = self._make_residual(block, self.num_feats, 1)
        self.maxpool = nn.MaxPool2d(2, stride=2)

        # build hourglass modules
        ch = self.num_feats * block.expansion
        self.hg = Hourglass(block, num_blocks, self.num_feats, 3)
        self.conv_up_1 = self._conv(ch, int(ch / 2))
        self.conv_up_2 = self._conv(int(ch / 2), int(ch / 4))
        self.score = nn.Sequential(
            nn.Conv2d(int(ch / 4), num_classes, kernel_size=3, stride=1, padding=1),
            nn.Sigmoid()
        )

    def _conv(self, inplane, outplane, k=3, stride=1, padding=1):
        return nn.Sequential(
            nn.Conv2d(inplane, outplane, kernel_size=k, stride=stride,
                      padding=padding, bias=True),
            nn.BatchNorm2d(outplane),
            nn.ReLU(inplace=True)
        )

    def _make_residual(self, block, planes, blocks, stride=1):
        downsample = None
        if stride != 1 or self.inplanes != planes * block.expansion:
            downsample = nn.Sequential(
                nn.Conv2d(self.inplanes, planes * block.expansion,
                          kernel_size=1, stride=stride, bias=True),
            )
        layers = []
        layers.append(block(self.inplanes, planes, stride, downsample))
        self.inplanes = planes * block.expansion
        for i in range(1, blocks):
            layers.append(block(self.inplanes, planes))

        return nn.Sequential(*layers)

    def _make_fc(self, inplanes, outplanes):
        bn = nn.BatchNorm2d(inplanes)
        conv = nn.Conv2d(inplanes, outplanes, kernel_size=1, bias=True)
        return nn.Sequential(
            conv,
            bn,
            self.relu,
        )

    def forward(self, x):
        x = self.conv1(x)
        x = self.bn1(x)
        x = self.relu(x)

        x = self.layer1(x)
        x = self.maxpool(x)
        x = self.layer2(x)
        x = self.layer3(x)

        y = self.hg(x)
        y_1 = F.interpolate(y, scale_factor=2)
        y_1 = self.conv_up_1(y_1)
        y_2 = F.interpolate(y_1, scale_factor=2)
        y_2 = self.conv_up_2(y_2)
        score = self.score(y_2)

        return score


class EmbeddingNet(nn.Module):
    def __init__(self, pretrained=False, num_classes=1000):
        super(EmbeddingNet, self).__init__()
        self.backbone = resnet50(pretrained=pretrained, num_classes=1000)

    def forward(self, input):
        vec = self.backbone(input)
        return vec


class CFnetWithCrop(nn.Module):
    def __init__(self):
        super(CFnetWithCrop, self).__init__()
        self.crop_net = RegionCropNet(Bottleneck)
        self.feature_net = EmbeddingNet(pretrained=False)

    def _inference(self, input):
        score_map = self.crop_net(input)
        score_map = input * score_map
        vec = self.feature_net(score_map)
        return score_map, vec

    def _train(self, *input):
        anchor = input[0]
        pos = input[1]
        neg = input[2]

        anchor_map, anchor_out = self._inference(anchor)
        pos_map, pos_out = self._inference(pos)
        neg_map, neg_out = self._inference(neg)

        return anchor_out, pos_out, neg_out, anchor_map, pos_map, neg_map

    def forward(self, *input):
        if len(input) > 1:
            return self._train(*input)
        else:
            return self._inference(*input)


class CFnet(nn.Module):
    def __init__(self):
        super(CFnet, self).__init__()
        self.crop_net = RegionCropNet(Bottleneck)
        self.feature_net = EmbeddingNet(pretrained=True)

    def _inference(self, input):
        score_map = None
        vec = self.feature_net(input)
        return score_map, vec

    def _train(self, *input):
        anchor = input[0]
        pos = input[1]
        neg = input[2]

        anchor_map, anchor_out = self._inference(anchor)
        pos_map, pos_out = self._inference(pos)
        neg_map, neg_out = self._inference(neg)

        return anchor_out, pos_out, neg_out, anchor_map, pos_map, neg_map

    def forward(self, *input):
        if len(input) > 1:
            return self._train(*input)
        else:
            return self._inference(*input)


if __name__ == '__main__':
    import torch

    pdist = nn.PairwiseDistance(p=2)
    input1 = torch.randn(10, 20)
    input2 = torch.randn(20, 20)
    out = pdist(input1, input2)
    print(out.shape)
