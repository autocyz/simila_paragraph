# !/usr/bin/env python  
# -*- coding:utf-8 _*-  
""" 
@author: autocyz
@time: 2019-09-11
@contact: autocyz@163.com
"""
import cv2
import json
import os
from os.path import join as oj
from tqdm import tqdm
from club_model.pgn import PGN
from club_model.fashion_pose import FashionPose


def main():
    root = '/data/open_data/supplier_unique/labeled'
    file_json = '/data/open_data/supplier_unique/all.json'
    save_root = '/home/cyz/data/code/simila_paragraph/trash/pgn'

    os.environ["CUDA_VISIBLE_DEVICES"] = '0'

    with open(file_json, 'r') as f:
        img_dict = json.load(f)
    imgs = []
    for k, v in img_dict.items():
        imgs += v
    print(f"total {len(imgs)} imgs")
    params_pgn = dict(
        model_path='./zcripts/frozen_model_no_pyramid.pb',
        if_coord=True,
        w=400,
        h=400,
        gpu_memory_fraction=0
    )

    model = PGN(**params_pgn)
    # model.export_frozen_pb()
    tt = tqdm(imgs)
    fail_num = 0
    for one in tt:
        tt.set_description_str(f"fail_num: {fail_num}")
        tt.write(one)
        img = cv2.imread(oj(root, one))
        if img is None:
            fail_num += 1
            continue
        img = cv2.resize(img, (400, 400))
        parsing, _ = model.predict(img)
        save_path = oj(save_root, one.replace('.jpg', '_mask.jpg'))
        os.makedirs(os.path.split(save_path)[0], exist_ok=True)
        parsing = parsing * 255
        # parsing = cv2.resize(parsing, (256, 256), cv2.INTER_NEAREST)
        cv2.imwrite(save_path, parsing)


if __name__ == '__main__':
    main()
