# !/usr/bin/env python  
# -*- coding:utf-8 _*-  
""" 
@author: autocyz
@time: 2019-07-22
@contact: autocyz@163.com
"""

from torch.utils.data import Dataset
from collections import defaultdict
import os
import cv2
import numpy as np
import random


class FashionDataDir(Dataset):
    def __init__(self, root, file_list, input_size=(256, 256)):
        super(FashionDataDir, self).__init__()
        self.root = root
        self.input_size = input_size
        with open(file_list, 'r') as f:
            self.img_list = f.readlines()
            self.img_list = [s.strip() for s in self.img_list]

    @staticmethod
    def _load_img(img_name, size):
        img = cv2.imread(img_name)
        if img is None:
            print(f"!!!!!!!!!!!!!!!!\n{img_name} is None")
        img = cv2.resize(img, size)
        img = np.transpose(img, (2, 0, 1)) / 255
        img = img.astype(np.float32)
        return img

    def __getitem__(self, index):
        img_name = self.img_list[index]
        img_path = os.path.join(self.root, img_name)
        img = self._load_img(img_path, self.input_size)
        return img

    def __len__(self):
        return len(self.img_list)


class DeepFashionInShop(Dataset):
    def __init__(self, root, train_val_list, phase, input_size=(256, 256)):
        super(DeepFashionInShop, self).__init__()
        self.root = root
        self.phase = phase
        self.input_size = input_size
        self.train = defaultdict(list)
        self.train_list = list()
        self.train_items = list()
        self.val = defaultdict(list)
        self.val_list = list()

        with open(train_val_list, 'r') as f:
            self.all = f.readlines()[2:]

        if self.phase == 'train':
            for one in self.all:
                one = one.strip().split()
                if one[2] == 'train':
                    self.train[one[1]].append([one[0], one[1]])
            # some item have <2 image, delete it
            delete_item = [k for k, v in self.train.items() if len(v) < 2]
            for k in delete_item:
                del self.train[k]
            for k, v in self.train.items():
                self.train_list += v
            self.train_items = list(self.train.keys())
            print(f"train have [{len(self.train)} items] [{len(self.train_list)} imgs]")
        else:
            for one in self.all:
                one = one.strip().split()
                # val phase == 'query' or 'gallery'
                if one[2] != 'train':
                    self.val[one[1]].append(one)
                    self.val_list.append(one)

            print(f"val have [{len(self.val)} items] [{len(self.val_list)} imgs]")

        self._load_data = self._load_train if phase == 'train' else self._load_val

    @staticmethod
    def _load_img(img_name, size):
        img = cv2.imread(img_name)
        if img is None:
            print(f"!!!!!!!!!!!!!!!!\n{img_name} is None")
        img = cv2.resize(img, size)
        img = np.transpose(img, (2, 0, 1)) / 255
        img = img.astype(np.float32)
        return img

    def _load_train(self, index):
        anchor_img, anchor_item = self.train_list[index]
        pos_img = anchor_img
        while pos_img == anchor_img:
            pos_img = random.choice(self.train[anchor_item])[0]

        neg_item = anchor_item
        while neg_item == anchor_item:
            neg_item = random.choice(self.train_items)
        neg_img = random.choice(self.train[neg_item])[0]

        anchor = self._load_img(os.path.join(self.root, anchor_img), self.input_size)
        pos = self._load_img(os.path.join(self.root, pos_img), self.input_size)
        neg = self._load_img(os.path.join(self.root, neg_img), self.input_size)
        return anchor, pos, neg

    def _load_val(self, index):
        img = self.val_list[index][0]
        img = self._load_img(os.path.join(self.root, img), self.input_size)
        return img, index

    def __getitem__(self, index):
        return self._load_data(index=index)

    def __len__(self):
        if self.phase == 'train':
            return len(self.train_list)
        else:
            return len(self.val_list)
            # return 1269


if __name__ == '__main__':
    from torch.utils.data import DataLoader

    root = '/data/cyz_data/DeepFashion/In-shop_Clothes_Retrieval_Benchmark/Img'
    train_split = '/data/cyz_data/DeepFashion/In-shop_Clothes_Retrieval_Benchmark/Eval/list_eval_partition.txt'

    train = DeepFashionInShop(root, train_split, 'train')
    test = DeepFashionInShop(root, train_split, 'test')

    trainloader = DataLoader(train, shuffle=True, batch_size=15)
    valloader = DataLoader(test, shuffle=False, batch_size=15)

    # for i, (anchor_img, pos_img, neg_img) in enumerate(trainloader):
    #     print(anchor_img.shape)
    #     print(pos_img.shape)
    #     print(neg_img.shape)
    #     break

    for i, (img, index) in enumerate(valloader):
        # print(img)
        print(index)
        break
