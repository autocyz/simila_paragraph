# !/usr/bin/env python  
# -*- coding:utf-8 _*-  
""" 
@author: autocyz
@time: 2019-08-08
@contact: autocyz@163.com
"""
import torch
import torch.nn as nn
import pretrainedmodels
from torchvision.models.resnet import resnet50
import numpy as np


class Resnet50(nn.Module):
    def __init__(self, num_class=1, pretrained='imagenet', feature_out=False):
        super(Resnet50, self).__init__()
        self.feature_out = feature_out
        # self.backbone = pretrainedmodels.resnet50()
        self.backbone = resnet50(pretrained=True)
        self.classifer = nn.Sequential(
            nn.ReLU(inplace=True),
            nn.Linear(1000, num_class)
        )
        self.sigmoid = nn.Sigmoid()

    def forward(self, *input):
        f = self.backbone(*input)
        y = self.classifer(f)
        if not self.training:
            y = self.sigmoid(y)
        if self.feature_out:
            return f, y
        else:
            return y


class OCResnet50(nn.Module):
    def __init__(self, num_class=1, pretrained='imagenet'):
        super(OCResnet50, self).__init__()
        self.backbone = pretrainedmodels.resnet50(pretrained=pretrained)
        self.classifier = nn.Sequential(
            nn.ReLU(inplace=True),
            nn.Linear(1000, num_class)
        )
        self.sigmoid = nn.Sigmoid()

    def forward(self, *input):

        if self.training:
            return self._train(*input)
        else:
            return self._test(*input)

    def _test(self, x):
        f = self.backbone(x)
        y = self.classifier(f)
        y = self.sigmoid(y)
        return y

    def _train(self, *input):
        real = input[0]
        fake = input[1]
        real_f = self.backbone(real)
        # print(real_f[0, 0:100])
        fake = real_f + fake
        y = torch.cat([real_f, fake], dim=0)
        y = self.classifier(y)
        return y


class NASnetLarge(nn.Module):
    def __init__(self, num_class=1, pretrained=None):
        super(NASnetLarge, self).__init__()
        self.backbone = pretrainedmodels.nasnetalarge(num_classes=1000, pretrained=pretrained)
        self.classifier = nn.Sequential(
            nn.ReLU(inplace=True),
            nn.Linear(1000, num_class)
        )
        self.sigmoid = nn.Sigmoid()

    def forward(self, *input):
        y = self.backbone(*input)
        y = self.classifier(y)
        if not self.training:
            y = self.sigmoid(y)
        return y


class NASnetLargeTriplet(nn.Module):
    def __init__(self, num_class=1000, pretrained=None):
        super(NASnetLargeTriplet, self).__init__()
        self.backbone = pretrainedmodels.nasnetalarge(num_classes=1000, pretrained=pretrained)

    def forward(self, *input):
        if self.training:
            return self._train(*input)
        else:
            return self._test(*input)

    def _train(self, *input):
        anchor = input[0]
        pos = input[1]
        neg = input[2]
        anchor_f = self.backbone(anchor)
        pos_f = self.backbone(pos)
        neg_f = self.backbone(neg)
        return anchor_f, pos_f, neg_f

    def _test(self, *input):
        feature = self.backbone(*input)
        return feature


class SEResnext50(nn.Module):
    def __init__(self, num_class=1000, pretrained=None):
        super(SEResnext50, self).__init__()
        self.backbone = pretrainedmodels.se_resnext50_32x4d(num_classes=num_class, pretrained=pretrained)

    def forward(self, *input):
        self.backbone(*input)
