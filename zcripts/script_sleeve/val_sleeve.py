# !/usr/bin/env python  
# -*- coding:utf-8 _*-  
""" 
@author: autocyz
@time: 2019-07-23
@contact: autocyz@163.com
"""
import logging
import torch


def val(net, valloader, epoch, logger, device='cpu'):
    display = logger
    if isinstance(display, logging.Logger):
        display = display.info

    pos_correct_num = 0
    pos_predict_num = 0
    pos_num = 0
    neg_correct_num = 0
    neg_predict_num = 0
    neg_num = 0
    correct_num = 0
    total_num = 0

    net.eval()
    with torch.no_grad():
        for i, (img, label) in enumerate(valloader):
            if device:
                img = img.to(device)
            vec = net(img)
            if i % 10 == 0:
                display(f"get feature {(i+1)*len(label)}")

            vec = vec.cpu().reshape(-1)
            label = label.reshape(-1).int()
            vec = vec > 0.5
            vec = vec.int()

            pos_predict_num += torch.sum(vec).item()
            neg_predict_num += len(vec) - torch.sum(vec).item()

            pos_pre = vec[label == 1]
            pos_tru = label[label == 1]

            neg_pre = vec[label == 0]
            neg_tru = label[label == 0]

            correct_num += torch.sum(vec == label).item()
            total_num += len(label)
            pos_correct_num += torch.sum(pos_pre == pos_tru).item()
            pos_num += len(pos_pre)
            neg_correct_num += torch.sum(neg_pre == neg_tru).item()
            neg_num += len(neg_pre)

    acc_total = correct_num / total_num
    acc_pos = pos_correct_num / pos_num
    acc_neg = neg_correct_num / neg_num
    display(f"epoch [{epoch}] "
            f"acc_total: \t[{correct_num}/{total_num}] = {acc_total}\n "
            f"acc_pos: \t[{pos_correct_num}/{pos_num}] = {acc_pos}\n"
            f"acc_neg: \t[{neg_correct_num}/{neg_num}] = {acc_neg}\n"
            f"precision_pos: \t[{pos_correct_num}/{pos_predict_num}] = {pos_correct_num / pos_predict_num}\n"
            f"precision_neg: \t[{neg_correct_num}/{neg_predict_num}] = {neg_correct_num / neg_predict_num}\n"
            f"recall_pos: \t[{pos_correct_num}/{pos_num}] = {pos_correct_num / pos_num}\n"
            f"recall_neg: \t[{neg_correct_num}/{neg_num}] = {neg_correct_num / neg_num}")

    return acc_total


if __name__ == '__main__':
    from simila_paragraph.sample.deep_fashion import DeepFashionInShop
    from torch.utils.data import DataLoader
    from simila_paragraph.network.fashion_net import FashionNet

    root = '/data/cyz_data/DeepFashion/In-shop_Clothes_Retrieval_Benchmark/Img'
    train_split = '/data/cyz_data/DeepFashion/In-shop_Clothes_Retrieval_Benchmark/Eval/list_eval_partition.txt'

    test = DeepFashionInShop(root, train_split, 'test')
    valloader = DataLoader(dataset=test, shuffle=False, batch_size=100)

    net = FashionNet()
    net = net.cuda(0)
    val(net, valloader, 0, print, 'cuda:0')
