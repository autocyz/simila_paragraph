# !/usr/bin/env python  
# -*- coding:utf-8 _*-  
""" 
@author: autocyz
@time: 2019-09-17
@contact: autocyz@163.com
"""

import cv2
import torch
import os
import cv2
import numpy as np
import random
import json
from torch.utils.data import Dataset
from collections import defaultdict
from torch.utils.data import BatchSampler
from os.path import join as oj


class SupplierMS(Dataset):
    def __init__(self, root, file_json, phase, input_size=(256, 256)):
        super(SupplierMS, self).__init__()
        self.root = root
        self.M = 5
        self.phase = phase
        self.training = True if phase == 'train' else False
        self.input_size = input_size

        # BGR channel
        with open(file_json, 'r') as f:
            self.data_dict = json.load(f)

        self.data_list = []
        if self.phase == 'train':
            remove = [k for k, v in self.data_dict.items() if len(v) < self.M]
            for k in remove:
                self.data_dict.pop(k)

            for cluster, imgs in self.data_dict.items():
                self.data_list += [[os.path.join(cluster, img), cluster, 'train'] for img in imgs]
        else:
            for cluster, imgs in self.data_dict.items():
                tmp = []
                for i, img in enumerate(imgs):
                    stat = 'gallery'
                    if i == 0:
                        stat = 'query'
                    tmp.append([os.path.join(cluster, img), cluster, stat])
                self.data_list += tmp

        print(f"{self.phase} dataset have {len(self.data_dict)} items, {len(self.data_list)} imgs")
        self.items = list(self.data_dict.keys())

        self._load_data = self._load_train if self.phase == 'train' else self._load_test

    def _load_img(self, img_name, size):
        img = cv2.imread(img_name)
        if img is None:
            print(f'img {img_name} is None')
            raise RuntimeError(f'img {img_name} is None')
        img = cv2.resize(img, size)
        img = np.transpose(img, (2, 0, 1)) / 255
        img = img.astype(np.float32)
        return img

    def _load_train(self, index):
        imgs = random.sample(self.data_dict[self.items[index]], self.M)
        imgs = [self._load_img(os.path.join(self.root, self.items[index], img), self.input_size) for img in imgs]
        labels = [index] * self.M
        imgs = np.stack(imgs, axis=0)
        # print(imgs.shape)
        labels = np.asarray(labels)
        # print(labels.shape)
        return imgs, labels

    def _load_test(self, index):
        img = self.data_list[index][0]
        img = self._load_img(oj(self.root, img), self.input_size)
        return img, index

    def __getitem__(self, index):
        return self._load_data(index)

    def __len__(self):
        if self.training:
            return len(self.data_dict)
        else:
            return len(self.data_list)


if __name__ == '__main__':
    from torch.utils.data import DataLoader

    root = '/data/open_data/supplier_unique/resized256/labeled'
    file_json = '/data/open_data/supplier_unique/train.json'
    phase = 'train'

    dataset = SupplierMS(root, file_json, phase)
    dataloader = DataLoader(dataset, 5, True, num_workers=1, pin_memory=True)
    # data = next(dataloader)
    for i, data in enumerate(dataloader):
        print(type(data[0]))
        print(data[0].shape)
        print(data[1].shape)
        print(data[1])
        if i >= 0:
            break
