# !/usr/bin/env python  
# -*- coding:utf-8 _*-  
""" 
@author: autocyz
@time: 2019-07-22
@contact: autocyz@163.com
"""
import time
import torch
import os
import logging
import sys

sys.path.append(os.getcwd())
from tensorboardX import SummaryWriter
from torch.utils.data import DataLoader
from torch.optim.lr_scheduler import StepLR
from simila_paragraph.utils import get_lr, save_params
from simila_paragraph.sample.deep_fashion_attr import AttrSleeve
from simila_paragraph.utils.log import MyLogger
from torch.nn.modules import BCEWithLogitsLoss
from simila_paragraph import network
from scripts.script_sleeve.config import args_parse
from scripts.script_sleeve.val_sleeve import val

total_iter = 0


def train(net, trainloader, criterion, optimizer, epoch, writer, logger, device='cpu'):
    display = logger
    if isinstance(display, logging.Logger):
        display = display.info

    time4 = 0
    net.train()
    for i, (img, label) in enumerate(trainloader):
        time4_last = time4
        time0 = time.time()

        label = label.float()
        if device:
            img = img.to(device)
            label = label.to(device)

        time1 = time.time()
        predict = net(img).reshape(-1)
        time2 = time.time()

        total_loss = criterion(predict, label)
        time3 = time.time()

        optimizer.zero_grad()
        total_loss.backward()
        optimizer.step()

        time4 = time.time()

        global total_iter
        total_iter += 1

        writer.add_scalar('train', total_loss.item(), total_iter)

        if total_iter % params['display'] == 0:
            logger.info('\nEpoch [{:03d}/{:03d}]\tStep [{}/{}  {:5d}]\tLr [{}] \ttotal_loss {:.4f}\n'
                        'T_pre:{:.5f} T_for:{:.5f} T_loss: {:.5f} T_back:{:.5f}'.
                        format(epoch, params['epoch_num'], i, len(trainloader), total_iter, get_lr(optimizer),
                               total_loss.item(),
                               time0 - time4_last, time2 - time1, time3 - time2, time4 - time3))


if __name__ == "__main__":

    params = args_parse()
    date_dir = os.path.join(params['result_dir'], params['date'])
    logdir = os.path.join(date_dir, 'log')
    cpt_dir = os.path.join(date_dir, 'checkpoint')
    tmp_dir = os.path.join(date_dir, 'tmp')
    os.makedirs(date_dir, exist_ok=True)
    os.makedirs(logdir, exist_ok=True)
    os.makedirs(cpt_dir, exist_ok=True)
    os.makedirs(tmp_dir, exist_ok=True)

    logger = MyLogger(os.path.join(logdir, "train.log"), "simple").get_logger()

    logger.info(">>> loading dataset")
    trainset = AttrSleeve(params['data_dir'], params['train_list'], input_size=params['input_size'])
    testset = AttrSleeve(params['data_dir'], params['test_list'], input_size=params['input_size'])
    trainloader = DataLoader(trainset, batch_size=params['batch_size'],
                             shuffle=True, num_workers=params['num_workers'], pin_memory=True)
    valloader = DataLoader(testset, batch_size=params['val_batch'],
                           shuffle=False, num_workers=params['num_workers'], pin_memory=True)
    logger.info("<<< loading over")

    params["train_spu_nums"] = len(trainset)
    params['val_spu_nums'] = len(testset)
    save_params(logdir, 'parameter', params)

    net = getattr(network, params['net']['name'])(*params['net']['param'])
    device = torch.device('cpu')
    if params['use_gpu']:
        gpu = params['gpu']
        device_num = torch.cuda.device_count()
        if device_num < 2:
            gpu = 0
        device = torch.device(gpu)
        torch.backends.cudnn.enabled = True
        torch.backends.cudnn.benchmark = True
        net = net.to(device)
    if params['model']:
        logger.info(f">>> loading pre_trained model : {params['model']}")
        net.load_state_dict(torch.load(params['model']))
        logger.info("<<< loading over")

    optimizer = torch.optim.Adam(net.parameters(), lr=params['lr'], weight_decay=params['weight_decay'])
    lr_scheduler = StepLR(optimizer, step_size=params['step_size'], gamma=0.1)
    criterion = BCEWithLogitsLoss()
    writer = SummaryWriter(log_dir=logdir)

    best_acc = 0.0
    for epoch in range(params['epoch_num']):
        lr_scheduler.step()
        train(net, trainloader, criterion, optimizer, epoch, writer, logger, device=device)
        acc = val(net, valloader, epoch, logger, device=device)
        writer.add_scalar('val_acc', acc, epoch)
        if acc > best_acc:
            best_acc = acc
            torch.save(net.state_dict(),
                       os.path.join(cpt_dir, "epoch_{:03d}_acc_{:.4f}.pth".format(epoch, best_acc)))
